const functApp = {
  getAge: function (studentList) {
    const today = new Date();
    let functBirth = this.getObjectBirthDate(studentList.birthday);
    let age = today.getFullYear() - functBirth.getFullYear();
    var m = today.getMonth() - functBirth.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < functBirth.getDate())) {
      age--;
    }

    return age + " " + this.getAgeString(age);
  },

  getAgeString: function (age) {
    let count = age % 100;

    if (count >= 10 && count <= 20) {
      return "лет";
    } else {
      count = age % 10;
      if (count === 1) {
        return "год";
      } else if (count >= 2 && count <= 4) {
        return "года";
      } else {
        return "лет";
      }
    }
  },

  getObjectBirthDate: function (birthDay) {
    const birthDateStudent = new Date(birthDay);
    return birthDateStudent;
  },

  getBirthDate: function (studentList) {
    let functBirth = this.getObjectBirthDate(studentList.birthday);
    const yyyy = functBirth.getFullYear();
    let mm = functBirth.getMonth() + 1;
    let dd = functBirth.getDate();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }

    return dd + "." + mm + "." + yyyy;
  },

  getStudyStart: function (studentList) {
    const currentTime = new Date();
    let endStudyYear = Number(studentList.studyStart) + 4;
    let studyYearText = "";

    if (endStudyYear < currentTime.getFullYear()) {
      studyYearText = "закончил";
    } else if (endStudyYear >= currentTime.getFullYear()) {
      studyYearText =
        currentTime.getFullYear() - studentList.studyStart + " курс";
    } else {
      studyYearText = endStudyYear - studentList.studyStart + " курс";
    }
    return (
      studentList.studyStart + "-" + endStudyYear + " (" + studyYearText + ")"
    );
  },

  sortStudents: function (arr, prop, dir) {
    const studentsCopy = [...arr];
    return studentsCopy.sort(function (studentA, studentB) {
      if (!dir == false ? studentA[prop] < studentB[prop] : studentA[prop] > studentB[prop])
        return -1;
    });
  },

  filter: function (arr, prop, value) {
    let result = [],
      copy = [...arr];

    for (const item of copy) {
      if (String(item[prop]).includes(value)) {
        result.push(item);
      }
    }

    return result;
  },

  validation: function (form) {
    let result = true;

    function removeError(input) {
      const parent = input.parentNode;

      if (parent.classList.contains("error")) {
        parent.querySelector(".error-label").remove();
        parent.classList.remove("error");
        input.classList.add("mb-3");
      }
    }

    function createError(input, text) {
      const parent = input.parentNode;
      const errorLabel = document.createElement("label");
      errorLabel.classList.add("error-label");
      errorLabel.textContent = text;

      input.classList.remove("mb-3");
      parent.append(errorLabel);
      parent.classList.add("error");
    }

    form.querySelectorAll("input").forEach((input) => {
      removeError(input);

      if (input.dataset.minBirth) {
        if (input.value < input.dataset.minBirth) {
          removeError(input);
          createError(input, `Минимальная дата рождения 01.01.1900`);
          result = false;
        }
      }

      if (input.dataset.minStart || input.dataset.maxStart) {
        if (input.value < input.dataset.minStart || input.value > input.dataset.maxStart) {
          removeError(input);
          createError(input, `Год обучения не может быть меньше ${input.dataset.minStart} или больше ${input.dataset.maxStart}`);
          result = false;
        }
      }
      if (input.dataset.minLength) {
        if (input.value.length < input.dataset.minLength) {
          removeError(input);
          createError(input, `Минимальное кол-во символов ${input.dataset.minLength}`);
          result = false;
        }
      }
      if (input.value.trim() == "") {
        removeError(input);
        createError(input, "Поле обязательно к заполнению");
        result = false;
      }
    });

    return result;
  },
  createQueryDB: async function () {
    const response = await fetch("http://localhost:3000/api/students");
    const studentsItemList = await response.json();
    return studentsItemList;
  },
  createStudentsItem: async function (name, surename, lastname, birthDate, studyStart, faculty) {
    const response = await fetch("http://localhost:3000/api/students", {
      method: "POST",
      body: JSON.stringify({
        name: name,
        surname: surename,
        lastname: lastname,
        birthday: birthDate,
        studyStart: studyStart,
        faculty: faculty,
        get fio() {
          return this.surname + " " + this.name + " " + this.lastname;
        },
        get birth() {
          return (
            functApp.getBirthDate(this) + " (" + functApp.getAge(this) + ")"
          );
        },
        get studyStartString() {
          return functApp.getStudyStart(this);
        },
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const studentsItem = await response.json();

    if (!studentsItem.errors) {
    } else {
      for (let i = 0; i < studentsItem.errors.length; i++) {
        alert(`Запрос завершился ошибкой: ${studentsItem.errors[i].message}`);
      }
    }

    return studentsItem;
  },

  deleteStudentItem: function (id) {
      fetch(`http://localhost:3000/api/students/${id}`, {
    method: "DELETE",
  });
  }
};
