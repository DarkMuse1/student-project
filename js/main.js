let studentsList = [];

const $studentList = document.getElementById("list__students"),
  $studentListSortAll = document.querySelectorAll(".students__table th"),
  $studentListFilter = document.querySelectorAll(".form__filter");

let column = "fio",
  columnDir = true,
  filterData = "",
  filterValue = "";

function newStudent(studentList) {
  const $student = document.createElement("tr"),
    $fio = document.createElement("td"),
    $birthDate = document.createElement("td"),
    $studyStart = document.createElement("td"),
    $faculty = document.createElement("td"),
    $buttonDel = document.createElement("button");

  $student.id = `student-${studentList.id}`;

  $fio.textContent = studentList.fio;
  $birthDate.textContent = studentList.birth;
  $studyStart.textContent = studentList.studyStartString;
  $faculty.textContent = studentList.faculty;

  $buttonDel.classList.add("del__button");
  $buttonDel.setAttribute("title", "Удалить");
  $buttonDel.setAttribute("onclick", `deleteStudent(this.closest('#${$student.id}'))`);
  $buttonDel.innerHTML = "&times;";

  $studyStart.append($buttonDel);
  $student.append($fio);
  $student.append($faculty);
  $student.append($birthDate);
  $student.append($studyStart);

  return $student;
}

$studentListSortAll.forEach((element) => {
  element.addEventListener("click", function () {
    column = this.dataset.column;
    columnDir = !columnDir;
    let arrowSort = "";

    if (columnDir == true) {
      arrowSort = this.dataset.sort + " &#9650";
    } else {
      arrowSort = this.dataset.sort + " &#9660";
    }

    element.innerHTML = arrowSort;

    render();
  });
});

$studentListFilter.forEach((element) => {
  element.addEventListener("input", function () {
    filterData = this.dataset.filter;
    filterValue = document.getElementById(element.id).value;

    render();
  });
});

async function render(query = false) {
  if (!query) {
    studentsList = await functApp.createQueryDB();
  }
  let studentsCopy = [...studentsList];

  studentsCopy = functApp.sortStudents(studentsCopy, column, columnDir);

  studentsCopy = functApp.filter(studentsCopy, filterData, filterValue);

  $studentList.innerHTML = "";

  if (studentsCopy == "") {
    showNoStudent();
  } else {
    for (const student of studentsCopy) {
      $studentList.append(newStudent(student));
    }
  }
}

document.getElementById("add__students").addEventListener("submit", async function (event) {
    event.preventDefault();
    let nameVal = document.getElementById("name").value.trim(),
      surenameVal = document.getElementById("surename").value.trim(),
      lastnameVal = document.getElementById("lastname").value.trim(),
      birthDate = document.getElementById("birthDate").value,
      studyStartVal = Number(document.getElementById("studyStart").value),
      facultyVal = document.getElementById("faculty").value.trim();

    if (functApp.validation(this) == true) {
      studentsList.push(await functApp.createStudentsItem(nameVal, surenameVal, lastnameVal, birthDate, studyStartVal, facultyVal)
      );
      document.querySelectorAll("#add__students input").forEach((element) => {
        element.value = "";
      });
      document.querySelector(".modal").classList.remove("open");
    }
    render(true);
  });

document.getElementById("callModal").addEventListener("click", function () {
  document.querySelector(".modal").classList.add("open");
});

document.getElementById("close").addEventListener("click", function () {
  document.querySelector(".modal").classList.remove("open");
});

function deleteStudent(student) {
  if (!confirm("Вы уверены?")) {
    return;
  }
  student.remove()

  let button = document.querySelector('.del__button')
  let studentID = student.id.replace(/student-/g, '')

  functApp.deleteStudentItem(studentID)

  if (button == null) {
    showNoStudent();
  }
}

function showNoStudent() {
  let studentsNoTR = document.createElement("tr");
  let studentsNoTD = document.createElement("td");

  studentsNoTD.setAttribute("colspan", "4");
  studentsNoTD.classList.add("students__error");
  studentsNoTD.textContent = "Студентов нет";

  $studentList.append(studentsNoTR);
  studentsNoTR.append(studentsNoTD);
}

render();
